export default class Component {
	constructor(tagName, attribute, children) {
		this.tagName = tagName;
		this.attribute = attribute;
		this.children = children;
	}
	render() {
		if (!this.children) {
			return `<${this.tagName} ${this.renderAttribute()}"/>`;
		}

		if (!this.children && !this.attribute) {
			return `<${this.tagName}/>`;
		}

		if (!this.attribute) {
			return `<${this.tagName}>${this.renderChildren()}</${this.tagName}>`;
		}

		return `<${
			this.tagName
		} ${this.renderAttribute()}"> ${this.renderChildren()} </${this.tagName}>`;
	}

	renderAttribute() {
		return `${this.attribute.name}="${this.attribute.value}"`;
	}

	renderChildren() {
		if (this.children instanceof Array) {
			let child = '';
			for (let i = 0; i < this.children.length; i++) {
				if (this.children[i] instanceof Component) {
					console.log('instanceof component');
					child = child + this.children[i].render();
				} else {
					child = child + this.children[i];
				}
			}
			return child;
		}
		return this.children;
	}
}
