import PizzaThumbnail from '../components/PizzaThumbnail';
import Component from '../components/Component.js';

export default class PizzaList extends Component {
	#pizzas = null;

	set pizzas(value) {
		this.#pizzas = value;
		let list = ``;
		this.#pizzas.map(element => {
			list += new PizzaThumbnail(element).render();
		});
		super.children = list;
	}

	constructor(data) {
		let list = ``;
		data.map(element => {
			list += new PizzaThumbnail(element).render();
		});
		super('section', { name: 'class', value: 'pizzaList' }, list);
	}
}
