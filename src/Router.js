import data from './data.js';
import Component from './components/Component.js';

export default class Router {
	static titleElement;
	static contentElement;
	static routes;

	static navigate(path) {
		this.routes.forEach(element => {
			this.titleElement.innerHTML = new Component(
				'h1',
				null,
				element.title
			).render();
			this.contentElement.innerHTML = element.page.render();
		});
	}
}
